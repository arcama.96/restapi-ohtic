package com.example.ohticchallengepassengers;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddPassenger extends AppCompatActivity {

    EditText txtName, txtFlight, txtSeat;
    TextView txtError;
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_passenger);

        setComponents();

        Intent intent = getIntent();

        String url = intent.getStringExtra("url");

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    executenewPassenger(url, intent);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

            }
        });




    }
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private void executenewPassenger(String url, Intent intent) throws IOException, JSONException {

        if(txtFlight.getText().toString().isEmpty() || txtName.getText().toString().isEmpty() || txtSeat.getText().toString().isEmpty())
            txtError.setText("Name, Flight and Seat must be completed.");
        else{
            txtError.setVisibility(View.GONE);

            JSONObject Passenger = createJSONPassenger(new Passenger(txtName.getText().toString(), txtFlight.getText().toString(), txtSeat.getText().toString()));

            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(Passenger.toString(), JSON); // new
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
//            Response response = client.newCall(request).execute();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    call.cancel();
                    AddPassenger.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtError.setText(e.getMessage().toString());
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    final String myResponse = response.body().string();

                    AddPassenger.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtError.setText(myResponse);
                            if(response.isSuccessful()){
                                setResult(Activity.RESULT_OK, intent);
                                finish();
                            }
                        }
                    });

                }
            });

            /*RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("name", txtName.getText().toString())
                    .addFormDataPart("flight", txtFlight.getText().toString())
                    .addFormDataPart("seat", txtSeat.getText().toString())
                    .build();
            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    call.cancel();
                    AddPassenger.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtError.setText(e.getMessage().toString());
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    final String myResponse = response.body().string();

                    AddPassenger.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtError.setText(myResponse);
                            if(response.isSuccessful()){
                                //setResult(Activity.RESULT_OK, intent);
                                //finish();
                            }
                        }
                    });

                }
            });*/

        }
    }

    private void setComponents(){

        txtName = findViewById(R.id.txtNewName);
        txtFlight = findViewById(R.id.txtNewFlight);
        txtSeat = findViewById(R.id.txtNewSeat);

        txtError = findViewById(R.id.txtNewError);

        btnAdd = findViewById(R.id.btnNewPassenger);

    }


    private JSONObject createJSONPassenger(Passenger p) throws JSONException {

        JSONObject result = new JSONObject();

        result.put("name", p.getName());
        result.put("flight", p.getFlight());
        result.put("seat", p.getSeat());

        return result;
    }



}