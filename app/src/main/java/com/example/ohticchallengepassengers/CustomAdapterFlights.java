package com.example.ohticchallengepassengers;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;

public class CustomAdapterFlights extends BaseAdapter {

    static class ViewHolder
    {
        TextView txtFlight;
        ConstraintLayout constraintLayout;
    }

    private ArrayList<String> data;
    private LayoutInflater inflater = null;
    private String url;

    public CustomAdapterFlights(Context c, ArrayList<String> flights, String url){

        this.data = flights;
        inflater = LayoutInflater.from(c);
        this.url = url + "/passenger_per_flights/";

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.oneflight, null);

            holder = new ViewHolder();
            holder.txtFlight = (TextView) convertView.findViewById(R.id.txtOneFlight);
            holder.constraintLayout = (ConstraintLayout) convertView.findViewById(R.id.constFlight);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }


        String flight = data.get(position);

        holder.txtFlight.setText(flight);

        View finalConvertView = convertView;
        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(finalConvertView.getContext(), OneFlightActivity.class);
                intent.putExtra("flight", flight);
                intent.putExtra("url", url + flight);

                parent.getContext().startActivity(intent);
            }
        });

        return convertView;
    }
}
