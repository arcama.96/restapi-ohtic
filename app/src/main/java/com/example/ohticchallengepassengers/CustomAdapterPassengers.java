package com.example.ohticchallengepassengers;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapterPassengers extends BaseAdapter {

    static class ViewHolder
    {
        TextView txtPassenger, txtPFlight, txtPSeat;
    }

    private ArrayList<Passenger> data;
    private LayoutInflater inflater = null;

    public CustomAdapterPassengers(Context c, ArrayList<Passenger> passengers){

        this.data = passengers;
        inflater = LayoutInflater.from(c);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.onepassenger, null);

            holder = new ViewHolder();
            holder.txtPassenger = (TextView) convertView.findViewById(R.id.txtPassenger);
            holder.txtPFlight = (TextView) convertView.findViewById(R.id.txtPFlight);
            holder.txtPSeat = (TextView) convertView.findViewById(R.id.txtPSeat);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }




        holder.txtPassenger.setText(data.get(position).getName());
        holder.txtPFlight.setText(data.get(position).getFlight());
        holder.txtPSeat.setText(data.get(position).getSeat());



        return convertView;
    }
}
