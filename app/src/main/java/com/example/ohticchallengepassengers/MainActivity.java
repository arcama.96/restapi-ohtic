package com.example.ohticchallengepassengers;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    String url = "";

    ListView lstFlights, lstPassengers;
    Button  btnRefreshRequest;
    EditText txtIP;
    TextView txtTest;
    FloatingActionButton btnAdd;

    List<Passenger> passengers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setComponents();

        btnRefreshRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String IP = txtIP.getText().toString();

                if(!IP.isEmpty()){
                    url = IP;
                    try{
                        runFlights(url, "flights");
                        runPassengers(url, "passengers");
                        //runTest(url, "passengers");
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }
                else
                    Toast.makeText(MainActivity.this, "Text can't be empty", Toast.LENGTH_LONG);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(url.isEmpty())
                    Toast.makeText(MainActivity.this, "Empty url, try again", Toast.LENGTH_LONG);
                else{
                    Intent intent = new Intent(MainActivity.this, AddPassenger.class);
                    intent.putExtra("url", url + "/passengers");

                    startActivityForResult(intent,0 );
                }
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (0) : {
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(MainActivity.this, "Passenger added", Toast.LENGTH_LONG);
                    try {
                        runFlights(url, "flights");
                        runPassengers(url, "passengers");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
        }
    }




    private void setComponents(){

        lstFlights = findViewById(R.id.listviewflights);
        lstPassengers = findViewById(R.id.listviewpassengers);

        txtIP = findViewById(R.id.editTextTextPersonName);
        txtTest = findViewById(R.id.txtTest);


        btnAdd  = findViewById(R.id.floatingActionButton);
        btnRefreshRequest  = findViewById(R.id.btnRefresh);

    }

    void runTest(String url, String add_url) throws IOException {

        String completeURL = url + "/" + add_url;

        OkHttpClient client = new OkHttpClient();
        Request req = new Request.Builder()
                .url(completeURL)
                .build();

        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                txtTest.setText(e.getMessage().toString());
                            }
                        });
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String myResponse = response.body().string();
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtTest.setText(myResponse);
                    }
                });

            }
        });


    }

    void runPassengers(String url, String add_url) throws IOException {

        String completeURL = url + "/" + add_url;

        OkHttpClient client = new OkHttpClient();
        Request req = new Request.Builder()
                .url(completeURL)
                .build();

        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtTest.setText(e.getMessage().toString());
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String myResponse = response.body().string();
                try {
                    setCustomAdapterPassengers(myResponse, lstPassengers);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    void runFlights(String url, String add_url) throws IOException {

        String completeURL = url + "/" + add_url;

        OkHttpClient client = new OkHttpClient();
        Request req = new Request.Builder()
                .url(completeURL)
                .build();

        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Error getting flights", Toast.LENGTH_LONG);
                    }
                });

            }


            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String myResponse = response.body().string();
                try {
                    setCustomAdapterFlight(myResponse, lstFlights, url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    private void setCustomAdapterFlight(String myResponse, ListView lstFlights, String url) throws JSONException {

        ArrayList<String> flights = parseJSONFlights(myResponse);

        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CustomAdapterFlights customAdapterFlights = new CustomAdapterFlights(getBaseContext(), flights, url);
                lstFlights.setAdapter(customAdapterFlights);
            }
        });

    }

    private void setCustomAdapterPassengers(String myResponse, ListView lstPassengers) throws JSONException {

        ArrayList<Passenger> passengers = parseJSONPassenger(myResponse);

        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CustomAdapterPassengers customAdapterPassengers = new CustomAdapterPassengers(getBaseContext(), passengers);
                lstPassengers.setAdapter(customAdapterPassengers);
            }
        });

    }

    private ArrayList<Passenger> parseJSONPassenger(String myResponse) throws JSONException {

        ArrayList<Passenger> result = new ArrayList<>();

        JSONArray jsonArray = new JSONArray(myResponse);

        for(int i = 0; i<jsonArray.length(); i++){

            String name = jsonArray.getJSONObject(i).getString("name");
            String flight = jsonArray.getJSONObject(i).getString("flight");
            String seat = jsonArray.getJSONObject(i).getString("seat");

            Passenger p = new Passenger(name, flight, seat);
            result.add(p);

        }
        return result;

    }


    private ArrayList<String> parseJSONFlights(String myResponse) throws JSONException {

        ArrayList<String> result = new ArrayList<>();

        JSONArray jsonArray = new JSONArray(myResponse);

        for(int i = 0; i<jsonArray.length(); i++){

            result.add(jsonArray.get(i).toString());

        }
        return result;

    }

}