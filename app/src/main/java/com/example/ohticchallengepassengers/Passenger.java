package com.example.ohticchallengepassengers;

public class Passenger {

    private String name;
    private Long ID;
    private String flight;
    private String seat;

    @Override
    public String toString() {
        return "Passenger{" +
                "name='" + name + '\'' +
                ", flight='" + flight + '\'' +
                ", seat='" + seat + '\'' +
                '}';
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public Passenger(String name, String flight, String seat) {
        this.name = name;
        this.flight = flight;
        this.seat = seat;
    }
}
