package com.example.ohticchallengepassengers;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OneFlightActivity extends AppCompatActivity {

    ListView lstPassengers;
    TextView txtPassxFlight, txtError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_flight);

        Intent intent = getIntent();
        String flight = intent.getStringExtra("flight");

        String url = intent.getStringExtra("url");

        setComponents();

        try{
            txtPassxFlight.setText(flight);
            runPassengersxFlight(url);
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    private void setComponents(){

        lstPassengers = findViewById(R.id.lstPassengersxFlight);
        txtPassxFlight = findViewById(R.id.txtPassxFlight);
        txtError = findViewById(R.id.txtError);

    }

    void runPassengersxFlight(String completeURL) throws IOException {

        OkHttpClient client = new OkHttpClient();
        Request req = new Request.Builder()
                .url(completeURL)
                .build();

        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
                OneFlightActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtError.setText(e.getMessage().toString());
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String myResponse = response.body().string();
                try {
                    setCustomAdapterPassengers(myResponse, lstPassengers);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    private void setCustomAdapterPassengers(String myResponse, ListView lstPassengers) throws JSONException {

        ArrayList<Passenger> passengers = parseJSONPassenger(myResponse);

        OneFlightActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CustomAdapterPassengers customAdapterPassengers = new CustomAdapterPassengers(getBaseContext(), passengers);
                lstPassengers.setAdapter(customAdapterPassengers);
            }
        });

    }

    private ArrayList<Passenger> parseJSONPassenger(String myResponse) throws JSONException {

        ArrayList<Passenger> result = new ArrayList<>();

        JSONArray jsonArray = new JSONArray(myResponse);

        for(int i = 0; i<jsonArray.length(); i++){

            String name = jsonArray.getJSONObject(i).getString("name");
            String flight = jsonArray.getJSONObject(i).getString("flight");
            String seat = jsonArray.getJSONObject(i).getString("seat");

            Passenger p = new Passenger(name, flight, seat);
            result.add(p);

        }
        return result;

    }



}