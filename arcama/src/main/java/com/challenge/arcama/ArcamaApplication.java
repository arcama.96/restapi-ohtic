package com.challenge.arcama;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArcamaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArcamaApplication.class, args);
	}

}
