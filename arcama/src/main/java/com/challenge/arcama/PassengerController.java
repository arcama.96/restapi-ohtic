package com.challenge.arcama;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
class PassengerController {

  private final PassengerRepository repository;

  PassengerController(PassengerRepository repository) {
    this.repository = repository;
  }


  // Aggregate root
  // tag::get-aggregate-root[]
  @GetMapping("/passengers")
  List<Passenger> all() {
    return repository.findAll();
  }
  // end::get-aggregate-root[]

	
  @GetMapping("/flights") 
  List<String> allFlights() {
  
	  List<String> allFlights = new ArrayList<String>();
	  
	  for (Passenger p : repository.findAll()) 
	  { 
		  if(!allFlights.contains(p.getFlight()))
			  allFlights.add(p.getFlight()); 
	  }
	  
	  return allFlights; 
  }
  
  @GetMapping("/passenger_per_flights/{flight}") 
  List<Passenger> allPassengersPerFlights(@PathVariable String flight) {
  
	  List<Passenger> allPassengers = new ArrayList<Passenger>();
	  
	  for (Passenger p : repository.findAll()) 
	  { 
		  if(p.getFlight().equals(flight))
			  allPassengers.add(p); 
	  }
	  
	  return allPassengers; 
  }
	 
  
  @PostMapping("/passengers")   //POST call
  Passenger newPassenger(@RequestBody Passenger newPassenger) {
    return repository.save(newPassenger);
  }

  // Single item
  
  @GetMapping("/passengers/{id}")  //GET call
  Passenger one(@PathVariable Long id) {
    
    return repository.findById(id)
      .orElseThrow(() -> new PassengerNotFoundException(id));
  }

  @PutMapping("/passengers/{id}")   //PUT call
  Passenger replacePassenger(@RequestBody Passenger newPassenger, @PathVariable Long id) {
    
    return repository.findById(id)
      .map(passenger -> {
    	  passenger.setName(newPassenger.getName());
    	  passenger.setFlight(newPassenger.getFlight());
    	  passenger.setSeat(newPassenger.getSeat());
        return repository.save(passenger);
      })
      .orElseGet(() -> {
    	  newPassenger.setId(id);
        return repository.save(newPassenger);
      });
  }

  @DeleteMapping("/passenger/{id}")   //DELETE call
  void deletePassenger(@PathVariable Long id) {
    repository.deleteById(id);
  }
}