package com.challenge.arcama;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
class Passenger {

  private @Id @GeneratedValue Long id;
  private String name;
  private String flight;
  private String seat;

  Passenger() {}

  Passenger(String name, String flight, String seat) {

    this.name = name;
    this.flight = flight;
    this.seat = seat;
  }

  public Long getId() {
    return this.id;
  }

  public String getName() {
    return this.name;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFlight() {
	return flight;
}

public void setFlight(String flight) {
	this.flight = flight;
}

public String getSeat() {
	return seat;
}

public void setSeat(String seat) {
	this.seat = seat;
}

public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {

    if (this == o)
      return true;
    if (!(o instanceof Passenger))
      return false;
    Passenger passenger = (Passenger) o;
    return Objects.equals(this.id, passenger.id) && Objects.equals(this.name, passenger.name)
        && Objects.equals(this.flight, passenger.flight) && Objects.equals(this.seat, passenger.seat);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id, this.name, this.flight, this.seat);
  }

  @Override
  public String toString() {
    return "Passenger{" + "id=" + this.id + ", name='" + this.name + '\'' + ", flight='" + this.flight + '\'' + ", this.seat='" + this.seat + '\'' +'}';
  }
}