package com.challenge.arcama;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

  private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

  @Bean
  CommandLineRunner initDatabase(PassengerRepository repository) {

    return args -> {
      log.info("Creating " + repository.save(new Passenger("Francisco Perez Perez", "MAD", "1A")));
      log.info("Creating " + repository.save(new Passenger("Aurora García Blasco", "MAD", "1B")));
      log.info("Creating " + repository.save(new Passenger("Julio Fernandez García", "BCN", "1A")));
      log.info("Creating " + repository.save(new Passenger("Elena Garrido Herrero", "BCN", "1B")));
      log.info("Creating " + repository.save(new Passenger("Juan Perez Silva", "BCN", "1C")));
      log.info("Creating " + repository.save(new Passenger("Pedro de María Vazquez", "BCN", "1D")));
      log.info("Creating " + repository.save(new Passenger("Luis Sanchez Briones", "BCN", "2A")));
      log.info("Creating " + repository.save(new Passenger("Lucia Caro Mira", "BCN", "2B")));
      log.info("Creating " + repository.save(new Passenger("Sebastian Gutierrez Sendín", "ATH", "1A")));
      log.info("Creating " + repository.save(new Passenger("María García Perez", "ATH", "1B")));
      log.info("Creating " + repository.save(new Passenger("Arturo Castillo Maldonado", "ATH", "1C")));
      
    };
  }
}