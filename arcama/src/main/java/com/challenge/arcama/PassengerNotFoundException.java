package com.challenge.arcama;

public class PassengerNotFoundException extends RuntimeException {
	
	PassengerNotFoundException(Long id) {
	    super("Could not find passenger " + id);
	  }
}
